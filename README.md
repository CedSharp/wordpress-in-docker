# WordPress in Docker

I had issues using the default wordpress docker image, so I built my own!

## Configuration

### Permission

Nginx and php-fpm usually have their own user for running things, and it
makes local development a pain.

This project expects you to provide your UID (run `id -u`) and your username
(run `id -un`). Copy `.env.example` to `.env` and add your user info.

```
UID=1000
USER=johndoe
```

With this information, nginx and php-fpm will use your user, and there should
be no more file permission conflicts.

_NOTE_: If you change those values after running `docker compose up -d`, you
will have to rebuild the containers. You can use
`docker compose up -d --build php web` for that.

### Nginx configuration

You can edit `./nginx.conf` to your liking. The primary intention is to
change the server name so you can use whatever domain name you want.

Make sure the line `include /etc/nginx/wordpress.conf;` is present in your
server block otherwise all of the wordpress urls won't work.

## New Project

_NOTE_: For this to work, the `./www` must be empty!

Simply run `docker compose up -d`. It will start an nginx, mariadb, php-fpm
environment in which WordPress will be installed in `./www`, salt-keys injected
and default database values replaced.

## Existing Project

If the `wp-config.php` file is not found in `./www` then the script will
attempt to clone wordpress repository in `./www`. _NOTE_: This will fail if
`./www` contains any files! Make sure it's empty if you want wordpress to be
installed automatically.

If `wp-config.php` already exists, nothing happens and the website is assummed
to be properly configured.

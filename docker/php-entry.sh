#! /bin/sh

until echo '\q' | mysql -h db -u wp_user -pwp_secret wp_db; do
  >&2 echo 'Waiting for DB...'
  sleep 1;
done
echo 'Connected to DB!'

if [ ! -f /var/www/html/wp-config.php ]; then
  cd /var/www/html

  # Install WordPress
  git clone https://github.com/WordPress/WordPress.git .
  chown -R $UID:$UID .
  find . -type f -exec chmod 644 {} \;
  find . -type d -exec chmod 755 {} \;

  # Generate wp-config.php with salts
  salt="$(curl -sL https://api.wordpress.org/secret-key/1.1/salt)"
  grep -A 1 -B 50 'since 2.6.0' wp-config-sample.php > wp-config.php
  echo "$salt" >> wp-config.php
  grep -A 50 -B 3 'WordPress database table prefix' wp-config-sample.php >> wp-config.php

  # Insert database information
  sed -i "s/localhost/db/" wp-config.php
  sed -i "s/database_name_here/wp_db/" wp-config.php
  sed -i "s/username_here/wp_user/" wp-config.php
  sed -i "s/password_here/wp_secret/" wp-config.php
fi

# Continue normal execution
docker-php-entrypoint $@
